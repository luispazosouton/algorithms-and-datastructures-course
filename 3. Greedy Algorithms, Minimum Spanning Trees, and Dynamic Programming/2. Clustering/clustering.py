########################################################################
######################                 #################################
######################     Part 1      #################################
######################                 #################################
########################################################################

'''
Computing a max-spacing kkk-clustering.

The input data describes a distance function (equivalently, a complete graph with edge costs). It has the following format:

[number_of_nodes]

[edge 1 node 1] [edge 1 node 2] [edge 1 cost]

[edge 2 node 1] [edge 2 node 2] [edge 2 cost]

...

There is one edge (i,j)(i,j)(i,j) for each choice of 1≤i<j≤n1 \leq i \lt j \leq n1≤i<j≤n, where nnn is the number of nodes.

For example, the third line of the file is "1 3 5250", indicating that the distance between nodes 1 and 3 (equivalently, the cost of the edge (1,3)) is 5250. Distances are positive, but not necessarily distinct.

The aim of this problem is to run the clustering algorithm where the target number kkk of clusters is set to 4. What is the maximum spacing of a 4-clustering?
'''

import time

# Define Datastructure Union-Find for creating the clusters
class unionFind():
    
    def __init__( self ):
        self.links = dict()
        self.invertedLinks = dict()

    # Add node

    def addNode( self, node ):
        self.links[ node ] = node
        self.invertedLinks[ node ] = [ node ]

    # Add new edge 
    def addEdge( self, edge ):
        
        # Assign nodes
        node1, node2 = edge
        
        # If none is stored
        if node1 not in self.links and node2 not in self.links:
            self.links[ node1 ] = node1
            self.links[ node2 ] = node1
            
            # Add to reversed links
            if node1 not in self.invertedLinks:
                self.invertedLinks[ node1 ] = [ node1, node2 ]
            else:
                self.invertedLinks[ node1 ] += [node1, node2 ]
        
        # If only one is stored, add to leader of the other one
        elif node1 not in self.links and node2 in self.links:
            
            # Get leader of existing node
            leader = self.links[ node2 ]
            
            # Add to new node
            self.links[ node1 ] = leader
            
            # Add new node to reversed links
            self.invertedLinks[ leader ].append( node1 )

        # Same with opposite
        elif node2 not in self.links and node1 in self.links:
            
            # Get leader of existing one
            leader = self.links[ node1 ]

            # Add to new node
            self.links[ node2 ] = leader
        
            # Add new node to reversed links
            self.invertedLinks[ leader ].append( node2 )

        # If both are stored and in different blops
        else:
            leader1, leader2 = self.links[ node1 ], self.links[ node2 ]
            
            # If different
            if leader1 != leader2:
                
                # Obtain items contained:
                nodesInLeader1, nodesInLeader2 = self.invertedLinks[ leader1 ], self.invertedLinks[ leader2 ]

                oldLeader, newLeader, nodesToReWire = min( ( leader1, leader2, nodesInLeader1 ), ( leader2, leader1, nodesInLeader2 ), key = lambda x: len( x[2] ) )

                # Re-wire nodes to leader
                for node in nodesToReWire:
                    self.links[ node ] = newLeader

                # Add nodes to inverse
                self.invertedLinks[ newLeader ] += nodesToReWire

                # Delete old blop
                del self.invertedLinks[ oldLeader ]

    # Find node in hashtable
    def find( self, node ):
        return self.links[ node ]

    def cycleCheck( self, edge ):
        node1, node2 = edge
        return node1 in self.links and node2 in self.links and self.links[ node1 ] == self.links[ node2 ]

    # Number of clusters
    def nClusters( self ):
        return len( self.invertedLinks.keys() )



######## Function to read input data #############
def dataLoader( Filename ):
    
    import csv

    data = list() 
    with open( Filename ) as file:
        for index, row in enumerate( csv.reader( file, delimiter='\n' ) ):
            
            # Get number of items from first row
            if index == 0:
                nitems = list( map( int, row[ 0 ].split() ) )
            
            # Extract weight and length of each item and add to data
            else:
                data.append( list( map( int, row[0].split() ) ) )

    return nitems, data

####################### Main ################################

# Record time
time0 = time.time()

########## Load Data #################
nitems, data = dataLoader( 'ClusteringDataPart1.txt' )

# Sort data based on cost of edges
data.sort( key = lambda x: x[2], reverse = False )


############# Create Clusters ############

# Initialize clusters with all nodes as individual ones
clusters = unionFind()

# Obtain nodes
nodes = set()
for edge in data:
    node1, node2, cost = edge
    # Add nodes to cluster
    clusters.addNode( node1 )
    clusters.addNode( node2 )

# Define wanted number of clusters
aimedClusters = 4 
T = list()
for edge in data:

    # Unpackdata
    node1, node2, cost = edge

    # If the edge doesn't introduce a cycle
    if not clusters.cycleCheck( [ node1, node2 ] ):
        clusters.addEdge( [ node1, node2 ] )
        T.append( (node1, node2) )
    
    if clusters.nClusters() <= aimedClusters:
        break

########## Find max distance #########
mincost = float('inf')
for edge in data:
    node1, node2, cost = edge

    if clusters.find( node1 ) != clusters.find( node2 ):
        mincost = min( mincost, cost )

########### Plot result ###############
print( 'Part I' )
print( 'Maximum spacing for 4 clusters: ', mincost )
print( 'Computation time: ', round( time.time() - time0, 1 ), ' seconds\n' )


######################################################
###############                    ###################
###############       Part 2       ###################
###############                    ###################
######################################################

'''
In this part the input data is MUCH larger. So big, in fact, that the distances (i.e., edge costs) are only defined implicitly, rather than being provided as an explicit list.

The format is:

[# of nodes] [# of bits for each node's label]

[first bit of node 1] ... [last bit of node 1]

[first bit of node 2] ... [last bit of node 2]

...

For example, the third line of the file "0 1 1 0 0 1 1 0 0 1 0 1 1 1 1 1 1 0 1 0 1 1 0 1" denotes the 24 bits associated with node #2.

The distance between two nodes uuu and vvv in this problem is defined as the Hamming distance--- the number of differing bits --- between the two nodes' labels. For example, the Hamming distance between the 24-bit label of node #2 above and the label "0 1 0 0 0 1 0 0 0 1 0 1 1 1 1 1 1 0 1 0 0 1 0 1" is 3 (since they differ in the 3rd, 7th, and 21st bits).

The question is: what is the largest value of kkk such that there is a kkk-clustering with spacing at least 3? That is, how many clusters are needed to ensure that no pair of nodes with all but 2 bits in common get split into different clusters?

NOTE: The graph implicitly defined by the data file is so big can't be written explicitly, let alone sort the edges by cost. So I will just identify the smallest distances without explicitly looking at every pair of nodes.
'''

# Define function to load large amounts of data

def largeDataLoader( Filename ):
    itemsInfo, rawData = dataLoader( Filename )

    # Pre-process input data into integers
    data = list()
    for bitString in rawData:
        data.append( int( ''.join( str( i ) for i in bitString ), 2 ) )

    return itemsInfo, data

# Define function that eliminates specific bit from bit chain
def delItem( n, indices ):
    
    # Use bitwise operation to flip bits at given positions
    for index in indices:
        n |= 2**index
    return n


# Remove specific bit from dataset
def delIndexFromData( data, indices ):
    return [ delItem( n, indices) for n in data ]

# Find matches
def matchFinder( data, edgeSet = set() ):
    leaders = dict()
    for index, integer in enumerate( data ):
        # Check if registered
        if integer in leaders:
            edgeSet.add( ( leaders[ integer ], index ) )
        else:
            leaders[ integer ] = index
    
    return edgeSet


######################## Main #########################

# Get initial time
time0 = time.time()

### Load Data ####
itemsInfo, data = largeDataLoader( 'ClusteringDataPart2.txt' )

# Obtain information about data
nItems, nBits = itemsInfo

# Find edges with distance 0
edgeSet = matchFinder( data, set() )

# Find edges with distance 1
for bitPos in range( nBits ):
    edgeSet = edgeSet.union( matchFinder( delIndexFromData( data, [ bitPos ] ), edgeSet ) )

# Get combinations of pairs
combinations = [ ( n, i ) for n in range( nBits ) for i in range( n + 1, nBits ) ]

# Find edges within a distance of 2
for bitPositions in combinations:
    edgeSet = edgeSet.union( matchFinder( delIndexFromData( data, bitPositions ), edgeSet ) )

# Define clusters
clusters = unionFind()

# Create unitary clusters
for index in range( nItems ):
    clusters.addNode( index )

# Start melting edges
for edge in edgeSet:
    clusters.addEdge( edge )

# Print results
print( 'Part II' )
print( "Number of clusters within a distance of 2: ", len( clusters.invertedLinks ) )
print( 'Computation time: ', round( time.time() - time0 ), 'seconds' )
