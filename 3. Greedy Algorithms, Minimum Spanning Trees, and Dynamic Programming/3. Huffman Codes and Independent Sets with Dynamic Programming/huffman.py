import requests

####################### Part 1 ###########################

'''
In this programming problem I'll code up a greedy algorithm to create a Huffman code tree based on the frequency of 
symbols from a certain alphabet or code.

The input data is the frequency of a series of symbols, and it has the following format:

[number_of_symbols]

[weight of symbol #1]

[weight of symbol #2]

...

For example, the third line of the file is "6852892," indicating that the weight of the second symbol of the alphabet is 6852892.

What is the maximum length of a codeword in the resulting Huffman code?
'''

# Define simple tree datastructure that allows to combine two trees
class tree( ):

    def __init__( self, value = None ):
        self.value = value
        self.left = None
        self.right = None
    
    def combine( self, tree1, tree2 ):
        self.value = tree1.value + tree2.value
        self.left = tree1
        self.right = tree2

# Find two smallest
def extractTwoSmallest( data ):
    
    # Function to get smallest index
    def getSmallestIndex( data ):
        return min( enumerate( data ), key = lambda x: x[1].value )[ 0 ]

    # Find smallest and extract
    index1 = getSmallestIndex( data )
    tree1 = data.pop( index1 )
    
    # Find second smallest and extract
    index2 = getSmallestIndex( data )
    tree2 = data.pop( index2 )

    return tree1, tree2
    
# Return depth of tree
def depthTree( tree ):

    # Seed case
    if not tree:
        return 0

    return 1 + max( depthTree( tree.left ), depthTree( tree.right ) )

# Return minimum depth (or shortest path)
def minDepthTree( tree ):

    # Seed case
    if not tree:
        return 0

    return 1 + min( minDepthTree( tree.left ), minDepthTree( tree.right ) )

############################# Main ###############################

## Load data from url
url = 'https://d18ky98rnyall9.cloudfront.net/_eed1bd08e2fa58bbe94b24c06a20dcdb_huffman.txt?Expires=1548460800&Signature=Iwnk01xw4uueun1NplnTG2GivWCy1IN4aSjjMp7mari-pg8IW2DuQV7wGCj5V6ke5srFO1iP-bnR6Rb7X~rzeghu7k8hFOffaFGBZrUpk0emBvSsHHVp~GPTk4MSWCYUJm7r2xtwiaDbL4Gj-6IB2Cr~9DCWEVq-F-oYHtDU2h0_&Key-Pair-Id=APKAJLTNE6QMUY6HBC5A' 
r = requests.get( url )

# Note that split introduces an empty string at the end, which is removed
rawData = r.text.split( '\n' )[ : -1 ]

# Classify input data 
nitems = int( rawData.pop( 0 ) )
data = ( int( value ) for value in rawData ) 

# Create a bunch of tree objects
treeData = [ tree( weight ) for weight in data ]

# Run through data
while len( treeData ) > 1:

    # Get two trees with smallest weights
    tree1, tree2 = extractTwoSmallest( treeData )
    
    # Create newTree based on combining those other two
    newTree = tree()
    newTree.combine( tree1, tree2 )

    # Append new tree to data
    treeData.append( newTree )


# Print longest code word 
# Length of code word is equal to depth of tree minus 1, since we only count branches
print( 'Part 1: ', 'Maximum length of Huffman codeword: ', depthTree( treeData[ 0 ] ) - 1 )
print( 'Part 2: ', 'Minimum length of Huffman codeword: ', minDepthTree( treeData[ 0 ] ) - 1 )





#################### Part 2 #########################

'''
Here I'll code up a dynamic programming algorithm for computing a maximum-weight independent set of a path graph.

The input file describes the weights of the vertices in a path graph (with the weights listed in the order in which vertices appear in the path). It has the following format:

[number_of_vertices]

[weight of first vertex]

[weight of second vertex]

...

For example, the third line of the file is "6395702," indicating that the weight of the second vertex of the graph is 6395702.

The task in this problem is to run the dynamic programming algorithm and reconstruct the optimal independent set.
The question is: of the vertices 1, 2, 3, 4, 17, 117, 517, and 997, which ones belong to the maximum-weight independent set? (By "vertex 1" we mean the first vertex of the graph---there is no vertex 0.) The solution will be given as a 8-bit string, where the ith bit should be 1 if the ith of these 8 vertices is in the maximum-weight independent set, and 0 otherwise. For example, if the vertices 1, 4, 17, and 517 are in the maximum-weight independent set and the other four vertices are not, then the solution would be 10011010.
'''
################# Main ###############

## Load data from url
url = 'https://d18ky98rnyall9.cloudfront.net/_790eb8b186eefb5b63d0bf38b5096873_mwis.txt?Expires=1548460800&Signature=NPuWEv6Xn8Ci7bZhkbsKesjt7ZwAn9RjuJ6n71fmlrKSCahwuELXC8lYEH5c-I46h7OGl51llDzuyNZZLW3HRL6l1KzXWtbZ6nzH3uboWO7RonlPVKNzdqykOmIGnB65ZdBiYqJualdoUJ23RTGGetki5NyLrkxZmgMyXqwTAFQ_&Key-Pair-Id=APKAJLTNE6QMUY6HBC5A'
r = requests.get( url )
rawData = r.text.split( '\n' )

# Assign to variables
nitems = int( rawData.pop( 0 ) )
data = [ int( value ) for value in rawData if value != '' ]

# Dictionary with solutions
sol = { -1: 0, 0 : 0, 1 : data[ 0 ] }

# Run through dataset
for i in range( 2, len( data ) + 1 ):
    sol[ i ] = max( sol[ i - 1], sol[ i - 2] + data[ i - 1 ] )

# Create set of solution by running backwards
independentSet = set()
i = len( data )
while i >= 1:
    # Check if included
    if  sol[ i - 1 ] >= sol[ i - 2 ] + data[ i - 1 ]:
        i -= 1
    else:
        independentSet.add( i - 1 )
        i -= 2

# Format solution as requested
vertexPositions = [ 1, 2, 3, 4, 17, 117, 517, 9971 ]
indices = ( i - 1 for i in vertexPositions )

# Compute final solution
solution = ''.join( ( str( 1 ) if index in independentSet else str( 0 ) for index in indices ) )
print( 'Part 3: ', solution )

