###### Part 1 ######
'''
In this programming problem I'll code up a greedy algorithm for minimizing the weighted sum of completion times.

The input data describes a set of jobs with positive and integral weights and lengths. It has the format:

[number_of_jobs]

[job_1_weight] [job_1_length]

[job_2_weight] [job_2_length]

...

For example, the third line of the file is "74 59", indicating that the second job has weight 74 and length 59.

The task of this program is to schedule jobs in decreasing order of the difference (weight - length), keeping in mind that this is not an optimal method. 

If two jobs have equal difference (weight - length), the program schedules the job with higher weight first. 

Finally, it reports the sum of weighted completion times of the resulting schedule.
'''

## Function to read input data ##
def dataLoader( Filename ):
    
    import csv

    data = list() 
    with open( Filename ) as file:
        for index, row in enumerate( csv.reader( file, delimiter='\n' ) ):
            
            # Get number of items from first row
            if index == 0:
                nitems = list( map( int, row[ 0 ].split() ) )
            
            # Extract weight and length of each item and add to data
            else:
                data.append( list( map( int, row[0].split() ) ) )

    return nitems, data
            

## Modified Quicksort to break ties with higher weight ##
def quickSort( lst ):
    
    # Define seed case
    if len( lst ) < 1:
        return lst

    # Else use recursivity
    else:
        
        # Define pivot as first item in list
        pivot = lst[0]
        
        # Define priority and weight of pivot
        priorPivot = pivot[ 2 ]
        weightPivot = pivot[ 0 ]
        
        # Create lists for small, higher and equal values
        a, c = list(), list()
        b = [ pivot ]
        
        # Run through elements in list
        for elem in lst[ 1: ]:
            
            # Sort based on priority
            priority = elem[ 2 ]
            
            if priority > priorPivot:
                a.append( elem )

            elif priority < priorPivot:
                c.append( elem )
            
            elif priority == priorPivot:
                
                # Break tie with weights
                weight = elem[ 0 ]
                
                if weight > weightPivot:
                    a.append( elem )

                elif weight < weightPivot:
                    c.append( elem )

                else:
                    b.append( elem )

        return quickSort( a ) + b + quickSort( c )


# Function to add priority to data based on given function
def addPriorityToData( data, priorityFunction ):

    # Run through data adding priority
    for elem in data:

        if len( elem ) == 2:
            elem.append( priorityFunction( elem ) )
        
        elif len( elem ) == 3:
            elem[ 2 ] = ( priorityFunction( elem ) )
    
    return data

# Define function to get weighted completion times
def weightCompletionTime( data ):

    # Sum of weighted completion times
    cTimes, weightedCompletion = 0, 0

    for elem in data:
        weight = elem[ 0 ]
        cTimes += elem[ 1 ]
        
        # Obtain weighted completion time
        weightedCompletion += weight * cTimes
    
    return weightedCompletion


#### Process data ####
nitems, data = dataLoader( 'Jobs.txt' )

# Define priority function for cases 1 and 2 
priorityFunction1 = lambda elem: elem[ 0 ] - elem[ 1 ]
priorityFunction2 = lambda elem: elem[ 0 ]/elem[ 1 ]

# Obtain results for both cases
result1 = weightCompletionTime( quickSort( addPriorityToData( data, priorityFunction1 ) ) )
result2 = weightCompletionTime( quickSort( addPriorityToData( data, priorityFunction2 ) ) )

# Print results
print( result1, result2 )


######### Part 2 #########
"""
In this programming problem I'll code up Prim's minimum spanning tree algorithm.


This file describes an undirected graph with integer edge costs. It has the format

[number_of_nodes] [number_of_edges]

[one_node_of_edge_1] [other_node_of_edge_1] [edge_1_cost]

[one_node_of_edge_2] [other_node_of_edge_2] [edge_2_cost]

...

For example, the third line of the file is "2 3 -8874", indicating that there is an edge connecting vertex #2 and vertex #3 that has cost -8874.

You should NOT assume that edge costs are positive, nor should you assume that they are distinct.

Your task is to run Prim's minimum spanning tree algorithm on this graph. You should report the overall cost of a minimum spanning tree --- an integer, which may or may not be negative --- in the box below.

IMPLEMENTATION NOTES: This graph is small enough that the straightforward O(mn) time implementation of Prim's algorithm should work fine. OPTIONAL: For those of you seeking an additional challenge, try implementing a heap-based version. The simpler approach, which should already give you a healthy speed-up, is to maintain relevant edges in a heap (with keys = edge costs). The superior approach stores the unprocessed vertices in the heap, as described in lecture. Note this requires a heap that supports deletions, and you'll probably need to maintain some kind of mapping between vertices and their positions in the heap.
"""
# Load data
nItems, edges = dataLoader( "Edges.txt" )

# Extract number of nodes and edges
nNodes, nEdges = nItems

# Create set of vertices
V = set()
for row in edges:
    for node in row[ :2 ]:
        V.add( node )

# Vertices explored so far (Initialize with first node )
X = set()
X.add( edges[ 0 ][ 0 ] )

# Edges in the tree
T = list()
while X != V:
    
    # Define seed minimum edge
    minEdge = [ 0, 0, float( 'inf' ) ]
    
    # Scan through edges to find minimum
    for index, edge in enumerate( edges ):
        node1, node2, cost = edge
        
        if node1 in X and node2 not in X or node2 in X and node1 not in X:
            minEdge = min( minEdge, edge, key = lambda x: x[ 2 ] )

    # Add smallest edge to tree
    T.append( minEdge )

    # Add new node to set of explored vertices
    X.add( minEdge[ 0 ] )
    X.add( minEdge[ 1 ] )

# Get cost
totalCost = sum( edge[ 2 ] for edge in T )

print( 'Total Cost: ', totalCost )
