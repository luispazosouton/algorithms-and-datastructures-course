'''
In this programming problem and the next you'll code up the knapsack algorithm from lecture.

Let's start with a warm-up. Download the text file below.
knapsack1.txt

This file describes a knapsack instance, and it has the following format:

[knapsack_size][number_of_items]

[value_1] [weight_1]

[value_2] [weight_2]

...

For example, the third line of the file is "50074 659", indicating that the second item has value 50074 and size 659, respectively.

You can assume that all numbers are positive. You should assume that item weights and the knapsack capacity are integers.

In the box below, type in the value of the optimal solution.

ADVICE: If you're not getting the correct answer, try debugging your algorithm using some small test cases. And then post them to the discussion forum!
'''

import time
import requests

# Recursive function for knapsack
def knapsack( knapsize, listOfItems, cache = dict() ):
    
    if not listOfItems or knapsize <= 0:
        return 0
    
    # Get weight and value of current item
    n = len( listOfItems )
    value, weight = listOfItems[ 0 ]

    # Define identifier
    identifier = ( knapsize, n )
    
    # Check if in cache
    if identifier in cache:
        return cache[ identifier ]
    else:
        # Get result
        if knapsize - weight >= 0:
            result = max( knapsack( knapsize, listOfItems[ 1: ] ), knapsack( knapsize - weight, listOfItems[ 1: ]  ) + value  )
        else:
            result = knapsack( knapsize, listOfItems[ 1: ] )

        # Cache result
        cache[ identifier ] = result

    # Returns highest value possible
    return result

# Create knapsack class and solves problem bottom-up with a 
# pre-processing step to avoid calculating unneccessary items
class knapsackClass():
    
    def __init__( self, maxKnapSize, listOfItems ):
        self.cache = dict()
        self.maxKnapSize = maxKnapSize
        self.listOfItems = listOfItems
        self.counter = 0

        # Preprocess data
        self.preprocess( knapSize, listOfItems )

    def preprocess( self, knapSize, listOfItems ):
        
        self.counter += 1
        
        # Define key parameters
        n = len( listOfItems ) - 1
        
        # Run DFS to get relevant positions
        stack = [ ( n, knapSize ) ]

        while stack:

            n, knapSize = stack.pop()
            if n >= 0 and knapSize >= 0:

                # Current item weight
                weight = listOfItems[ n ][ 1 ]
                
                # If already looked into, continue with loop
                if n in self.cache and knapSize in self.cache[ n ]:
                    continue

                # Check if in cache and add otherwise
                if n in self.cache and knapSize not in self.cache[ n ]:
                    self.cache[ n ].add( knapSize )
                elif n not in self.cache:
                    self.cache[ n ] = { knapSize }
        
                # Add items to stack        
                stack.extend( [ (n - 1, knapSize), (n - 1, knapSize - weight) ] )


    def knapsackValue( self, maxKnapSize, listOfItems ):
        
        # Initialize A
        A = dict()

        # Run through all items
        for n in range( len( listOfItems ) ):
            
            # Get item value and weight
            value, weight = listOfItems[ n ]
            
            # Go through kapsack size's of interest
            for size in self.cache[ n ]:

                if n - 1 < 0:
                    result1, result2 = 0, 0
                    
                elif size - weight < 0:
                    result1 = A[ n - 1, size ]
                    result2 = 0
                else:
                    result1 = A[ n - 1, size ]
                    result2 = A[ n - 1, size - weight ] + value

                A[ n , size ] = max( result1, result2 ) 

        return A[ n, size ]



#################### Main - Part 1 ##################
'''
This problem also asks you to solve a knapsack instance, but a much bigger one.
Download the text file below.
knapsack_big.txt
This file describes a knapsack instance, and it has the following format:
[knapsack_size][number_of_items]
[value_1] [weight_1]
[value_2] [weight_2]
...
For example, the third line of the file is "50074 834558", indicating that the second item has value 50074 and size 834558, respectively. As before, you should assume that item weights and the knapsack capacity are integers.
This instance is so big that the straightforward iterative implemetation uses an infeasible amount of time and space. So you will have to be creative to compute an optimal solution. One idea is to go back to a recursive implementation, solving subproblems --- and, of course, caching the results to avoid redundant work --- only on an "as needed" basis. Also, be sure to think about appropriate data structures for storing and looking up solutions to subproblems.
In the box below, type in the value of the optimal solution.
'''
# Print start
print( 'Part 1 -- Small Problem \n' )

# Load data
r = requests.get( 'https://d18ky98rnyall9.cloudfront.net/_6dfda29c18c77fd14511ba8964c2e265_knapsack1.txt?Expires=1548547200&Signature=ChBlHexL9i03ob0tSRBNmOJW4Ysc9NVax-0azxzZIUimfu4csq~59tptGVf6ILHr22Pf3IRyJ4PjlYT0yNYQhVYJ82TwXUwGPuynCzOlrVMtUmllNKLU14bULHNcLy4P6OpRI1RbQk5~YQVZm6UkmTJEcRI4pxpseRBIV9HIIgw_&Key-Pair-Id=APKAJLTNE6QMUY6HBC5A' )

'''
### For test cases only 
# Load tests
url = 'https://raw.github.com/beaunus/stanford-algs/master/testCases/course3/assignment4Knapsack/input_random_10_100_10.txt'
r = requests.get( url )
'''

# Parse raw data
rawData = r.text.split('\n')[:-1]

# Classify as list of lists
data = [ list( map( int, elem.split(' ') ) ) for elem in rawData ]

# Read Header
knapSize, nItems = data.pop(0)

# Take time
time0 = time.time()

# Initialize knapSack object and run knapsack
knapProblem = knapsackClass( knapSize, data )
resultBottomUp = knapProblem.knapsackValue( knapSize, data )
print( 'Maximum value (bottom-up approach): ', resultBottomUp )

# Print out time
print( 'Computation time: ', round( time.time() - time0, 2 ), 's', '\n' )

'''
### For test cases only
# Check output
out = requests.get( url.replace( 'input', 'output' ) )
print( 'Matches? ', 'Yes!' if int( out.text ) == resultBottomUp else 'No', '\n' )
'''

# Take time
time2 = time.time()

# Run second knapsack ( Recursive )
resultRecurse = knapsack( knapSize, data )
print( 'Maximum value (recursive approach): ', resultRecurse )

# Print time
print( 'Computation time: ', round( time.time() - time2, 2 ), 's', '\n' )

# Calculate sparsity
counter = 0
for key in knapProblem.cache:
    for item in knapProblem.cache[ key ]:
        counter += 1

'''
### For test cases only
# Check output
out = requests.get( url.replace( 'input', 'output' ) )
print( 'Matches? ', 'Yes!' if int( out.text ) == resultRecurse else 'No', '\n' )
'''



#################### Main - Part 2 ##################
'''
## Part 2
This problem also asks you to solve a knapsack instance, but a much bigger one.

Download the text file below.
knapsack_big.txt

This file describes a knapsack instance, and it has the following format:

[knapsack_size][number_of_items]

[value_1] [weight_1]

[value_2] [weight_2]

...

For example, the third line of the file is "50074 834558", indicating that the second item has value 50074 and size 834558, respectively. As before, you should assume that item weights and the knapsack capacity are integers.

This instance is so big that the straightforward iterative implemetation uses an infeasible amount of time and space. So you will have to be creative to compute an optimal solution. One idea is to go back to a recursive implementation, solving subproblems --- and, of course, caching the results to avoid redundant work --- only on an "as needed" basis. Also, be sure to think about appropriate data structures for storing and looking up solutions to subproblems.

In the box below, type in the value of the optimal solution.
'''
# Print start
print( 'Part 2 -- Large Problem\n' )

# Load data
r = requests.get( 'https://d18ky98rnyall9.cloudfront.net/_6dfda29c18c77fd14511ba8964c2e265_knapsack_big.txt?Expires=1548547200&Signature=WczjFWDdEIPqUAnbSd9EWaXHQUiX4VehR71s7RA3Kb8pocWotuydE5FL~lfRrH~5nBYjmc5oZ1IkAo2tij9yh8bE11hcc835LWzUIHPai9I3ixYvrWPcGQbQrPbjuUhWlEHhO7lYj5020khRSJvpEe90N8AzVkjj7W7zpApnkiA_&Key-Pair-Id=APKAJLTNE6QMUY6HBC5A' )

# Parse raw data
rawData = r.text.split('\n')[:-1]

# Classify as list of lists
data = [ list( map( int, elem.split(' ') ) ) for elem in rawData ]

# Read Header
knapSize, nItems = data.pop(0)

# Time checkpoint
time0 = time.time()

# Initialize knapSack object
knapProblem = knapsackClass( knapSize, data )

# Get and print maximum value
print( 'Maximum Value: ', knapProblem.knapsackValue( knapSize, data ) )

# Print computation time
print( 'Computation time for Part 2: ', round( time.time() - time0, 1 ), 's' )

# Calculate sparsity
counter = 0
for key in knapProblem.cache:
    for item in knapProblem.cache[ key ]:
        counter += 1

print( 'Sparsity: ', counter/( knapSize * nItems ), '\n' )

