############## Quicksort Exercise #####################

import numpy as np
import random as rand
import csv

########## Load the Data #############

lst2 = []; 
with open('Data.txt', 'rb') as csvfile:
  spamreader = csv.reader( csvfile, delimiter = '\n' )
  lst2 = [ int(row[0]) for row in spamreader if row != [] ]

#### Define useful functions ########

def exchange( ind1, ind2, lst ):
    lst[ind1], lst[ind2] = lst[ind2], lst[ind1]
    return lst

#### Define Quicksort Algorithm first element #####

def quicksort( lstin, pivot_type = 'first', com = 0 ):

  lst = []
  lst[:] = lstin[:]

  
  ## Seed ##
  if len( lst ) <= 1:
    return [lst, com]

  ## Recursive component ##
  else:
    
    ## Counter of comparisons
    com = com + len(lst) - 1

    ########## Select pivot #############

    # Function for median pivot
    def medianpivot( lst3 ):
      lst[:] = lst3[:] 
      # Middle index
      middle = (len(lst) - 1)/2
      
      # Sublist with three values to get median
      sublst = [ lst[0], lst[middle], lst[-1] ]
      dicti ={ 0 : 0, 1: middle, 2:-1 }
      
      # Get median value
      mean = sum( sublst[:] )/3.0
      redlst = [ abs(i - mean) for i in sublst ]
      pivot_val = sublst[ redlst.index( min( redlst) ) ]
      
      # Get index of median value and plug in dictionary
      pivot = dicti[ sublst.index(pivot_val) ]
      return pivot
    
    # Function for random pivot
    def randompivot( lst3 ):
      lst[:] = lst3[:]
      pivot = rand.randint(0, len(lst) - 1)
      return pivot
    

    # Define dictionary for pivot type
    pivotdict = { 'first' : 0, 'last' : -1, 'median' : medianpivot(lst), 'random' : randompivot(lst) }
    
    # Select pivot
    pivot = pivotdict[ pivot_type ] 
    

    ########### Exchange pivot with first element #############
    lst = exchange( 0, pivot, lst )
    pivot = 0
    i = 0;
    for j in range( 1, len(lst) ):
      if lst[j] <= lst[pivot]:
        lst = exchange( j, i + 1, lst )  
        i = i + 1
    
    ## Exchange pivot and last element of small array
    lst = exchange( pivot, i, lst )
    
    ## Call quicksort recursively
    quick1 = quicksort( lst[0:i], pivot_type )
    quick2 = quicksort( lst[i+1:], pivot_type )
    return [ quick1[0] + [ lst[i] ] + quick2[0], com + quick1[1] + quick2[1] ]



############# Main ##################


lst3 = []; 
with open('Data.txt', 'rb') as csvfile:
  spamreader = csv.reader( csvfile, delimiter = '\n' )
  lst3 = [ int(row[0]) for row in spamreader if row != [] ]

quick1 = quicksort( lst2, 'first' )
quickend = quicksort( lst2, 'last' )
quickmedian = quicksort( lst2, 'median' )
quickrand = quicksort( lst2, 'random' )

print quick1[0] == sorted(lst2)
print quickend[0] == sorted(lst2)
print quickmedian[0] == sorted(lst2)
print quickrand[0] == sorted(lst2)

print quick1[1]
print quickend[1]
print quickmedian[1]
print quickrand[1]

