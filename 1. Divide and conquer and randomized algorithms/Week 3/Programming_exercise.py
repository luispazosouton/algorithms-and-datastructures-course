############## Quicksort Exercise #####################

import numpy as np
import random
import csv

########## Load the Data #############

lst2 = []; 
with open('Data.txt', 'rb') as csvfile:
  spamreader = csv.reader( csvfile, delimiter = '\n' )
  lst2 = [ int(row[0]) for row in spamreader if row != [] ]

print lst2[0:50]

#### Define useful functions ########

def exchange( ind1, ind2, lst ):
    lst[ind1], lst[ind2] = lst[ind2], lst[ind1]
    return lst

#### Define Quicksort Algorithm first element #####

def quicksort_first( lst, com = 0 ):
  
  ## Seed ##
  if len( lst ) <= 1:
    return [lst, com]

  ## Recursive component ##
  else:
    
    com = com + len(lst) - 1

    ## Define pivot
    pivot = 0
    ## Exchange pivot with first element    
    lst = exchange( 0, pivot, lst )
    pivot = 0
    i = 0;
    for j in range( 1, len(lst) ):
      if lst[j] <= lst[pivot]:
        lst = exchange( j, i + 1, lst )  
        i = i + 1
    
    ## Exchange pivot and last element of small array
    lst = exchange( pivot, i, lst )
    
    ## Call quicksort recursively
    quick1 = quicksort_first( lst[0:i] )
    quick2 = quicksort_first( lst[i+1:] )
    return [ quick1[0] + [ lst[i] ] + quick2[0], com + quick1[1] + quick2[1] ]


#### Define Quicksort Algorithm last element #####

def quicksort_last( lst, com = 0 ):
  
  ## Seed ##
  if len( lst ) <= 1:
    return [lst, com]

  ## Recursive component ##
  else:
    
    com = com + len(lst) - 1

    ## Define pivot
    pivot = -1
    ## Exchange pivot with first element    
    lst = exchange( 0, pivot, lst )
    pivot = 0
    i = 0;
    for j in range( 1, len(lst) ):
      if lst[j] <= lst[pivot]:
        lst = exchange( j, i + 1, lst )  
        i = i + 1
    
    ## Exchange pivot and last element of small array
    lst = exchange( pivot, i, lst )
    
    ## Call quicksort recursively
    quick1 = quicksort_last( lst[0:i] )
    quick2 = quicksort_last( lst[i+1:] )
    return [ quick1[0] + [ lst[i] ] + quick2[0], com + quick1[1] + quick2[1] ]


quick1 = quicksort_last(lst2)

############# Main ##################


lst3 = []; 
with open('Data.txt', 'rb') as csvfile:
  spamreader = csv.reader( csvfile, delimiter = '\n' )
  lst3 = [ int(row[0]) for row in spamreader if row != [] ]

print lst2[0:50]
print lst3[0:50]

print lst2 == lst3
