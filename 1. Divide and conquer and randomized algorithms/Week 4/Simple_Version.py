import csv
import random
import copy
import mpi4py
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
print( comm, rank )
print( comm )
# Read graph
with open( 'Graph.txt' ) as file:
    reading = csv.reader( file, delimiter = '\t' ) 
  
    # Parse each line and build the graph into a hashtable
    graph = {}
    for row in reading:
        list_of_nums = [ int( string ) for string in row if string != '' ]
        graph[ list_of_nums[0] ] = set( list_of_nums[1:] )

##############################################
####################  ########################
#################  Main  #####################
####################  ########################
##############################################

# define the Vehicle class
class Graph:
    
    
    # Initialize class
    def __init__( self, graph ):
        self.graph = graph
        self.current_node = list( graph.keys() )[0]
        double_edge_counter = 0
        self.executions = 0

        # Function adds edges from given node
        def add_edges( node ):
            [ self.edges.append( ( node, set_elem ) ) for set_elem in graph[ node ] if ( set_elem, node ) not in self.edges ]
        
        # Define edges
        self.edges = [] 
        for elem in graph: add_edges( elem )

    ###########################################################
    ########## Random contraction algorithm ###################
    ###########################################################

    ############ Define edge deletion function ################

    def random_contraction( self ):
        
        # Reinitialize graph
        graph = copy.deepcopy( self.graph )
        edges = copy.deepcopy( self.edges )
        double_edge_counter = 0
        self.executions += 1
        print( 'Executions: ', self.executions )
        
        # Cache collapsing nodes
        self.cache_nodes = {}
        
        # Function to eliminate edge from graph
        def del_edge( graph, edges, double_edge_counter ):

        ######################## Remove Elements from Graph ################
            # Select random edge
            edge = random.choice( edges )
            
            # Remove edge from graph
            graph[ edge[0] ].remove( edge[1] )
            graph[ edge[1] ].remove( edge[0] )
            
            # Fuse nodes
            graph[ edge[0] ] = graph[ edge[0] ].union( graph[ edge[1] ] )
            
            # Keep track of where are nodes collapsing into
            # If node not in memo, add
            if edge[0] not in self.cache_nodes:
                self.cache_nodes[ edge[ 0 ] ] = []
            
            # Add new node
            self.cache_nodes[ edge[ 0 ] ] += [ edge[ 1 ] ]

            # Add sub_nodes attached to collapsing node
            if edge[1] in self.cache_nodes:
                    self.cache_nodes[ edge[ 0 ] ] += self.cache_nodes[ edge[ 1 ] ]
                    del self.cache_nodes[ edge[ 1 ] ]

        ################## Remove Elements from Edges List ################

            # Remove edge from set of edges
            edges.remove( edge )
            
            # Remove all edges connected to second element and redirect them
            ind_list_to_remove = []
            list_of_tuples_to_append = []
            
            for ind, tupl in enumerate( edges ):
                for ind2, elem in enumerate( tupl ):
                    
                    # If element found in tuple, remove tuple from list and add another one with the redirected element
                    if elem == edge[ 1 ]:
                        ind_list_to_remove.append( ind )
                        list_of_tuples_to_append.append( ( edge[ 0 ], tupl[ 1 - ind2 ] ) )
            
            # Remove elements from list
            for ind in sorted( ind_list_to_remove, reverse = True ):
                del edges[ ind ]

            # Append elements to list
            edges += list_of_tuples_to_append
            
            # Count self-loops
            self.self_loop_counter = 0
            indeces_to_del = []
            for index, elem in enumerate( edges ):
                if elem[0] == elem[1]: 
                    indeces_to_del.append( index )
                    self.self_loop_counter += 1
            
            # Remove self-loops from list of edges
            for ind in sorted( indeces_to_del, reverse = True ):
                del edges[ ind ] 
            
            # Redirect edges to first node
            for key, values in graph.items(): 
                
                if edge[1] in values:
                    values.remove( edge[1] )
                    
                    if edge[0] not in values:
                        values.add( edge[0] )

            # Remove second node
            graph.pop( edge[1] )
            

            # Count double edges
            memo = set();
            for index, elem in enumerate( edges ):
                if elem in memo or ( elem[1], elem[0] ) in memo:
                    double_edge_counter += 1
                else:
                    memo.add( elem )
            
            return [ edges, graph, double_edge_counter ]

  ####### Execute edge deletion until end result ####################
        while len( edges ) > double_edge_counter + 1:
            
            # Remove edge
            results = del_edge( graph, edges, 0 )
            
            # Update values
            edges = results[0]; graph = results[1]
            double_edge_counter = results[2]
        
        return [ len( edges ), self.cache_nodes ] 

    

mygraph = Graph( graph )
results = [ mygraph.random_contraction() for _ in range( 1000 ) ]

def find_min_cut( results ):
    
    # Initialize cache
    cache = [ results[0][0], results[0][1] ]

    # Loop through results to find minimum
    for index, elem in enumerate( results ):
        if elem[ 0 ] < cache[ 0 ]:
            cache[ 0 ] = elem[ 0 ]
            cache[ 1 ] = elem[ 1 ]
    
    # Return minimum cut
    return cache

cache = find_min_cut( results )

print('Number Edges Min Cut: ', cache[0]  )
print('Nodes collapsed for Min Cut: ', cache[1]  )
