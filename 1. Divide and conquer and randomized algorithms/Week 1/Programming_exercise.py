############## Multiplication algorithms #####################

import numpy as np
import random
import time
import matplotlib.pyplot as plt
import math

endrange = 2**4 
times = range(1, endrange);


## Number of entries
for n in range(1, endrange):
        
    ## create list of random numbers
    
    n1 = []
    
    for i in range(0, n+1 ):
        x = random.randint(0,9)
        n1.append(x)
    
    n2 = []
    
    for i in range(0, n+1 ):
        x = random.randint(0,9)
        n2.append(x)



#################### General functions #########################

####### Function takes vector and returns vector rotated #######

def rotvec( vec ):
  ## Final loop to re-arrange values
  rotatedvec = np.zeros( len( vec ) )
  
  for i in range( len( vec ) ):
      rotatedvec[ - ( i + 1 ) ] = vec[ i ]
  return rotatedvec


####### Make summation over matrix ########

def summation( prod ):
  
  cols = prod.shape[ 1 ]
  acc = 0; pass_val = 0
  result = np.zeros( cols + 1 )
                                                                
  ## Loop through matrices
  for j in range( cols ):
    acc = sum( prod[ :, -( 1 + j ) ] ) + pass_val
    result[ j ] = acc%10
    pass_val = ( acc - acc%10 )/10
  
  ## Add value at end of carry value
  result[ j + 1 ] = pass_val    
  
  return rotvec( result ) 


###########################################################
############### School algorithm ##########################
###########################################################

start_time = time.time()

############# School multiplication #######################


## Define matrix of nxm dimensions

ns = len( n2 )
m = len( n1 ) + len( n2 )

prod = np.zeros(( ns, m ))
result = np.zeros( m )

# First step of multiplication
for i in range( 1, len(n2) +1 ):
  for j in range(1, len(n1) +1 ):
    

    aux = n2[-i]*n1[-j] + prod[i-1, -j -i + 1 ]

    ## Add values to array
    prod[i-1, -j -i + 1] = aux%10
    prod[i-1, -j-1 -i + 1] = (aux - aux%10)/10


###############################################################
####################### Advanced Algorithm ####################

start_time = time.time()


def karatsuba( num1, num2 ):
   
    def countdigits( num ):
      
      # Count digits
      if num1 > 1:
        return int( math.ceil( math.log( num1, 10 ) ) )
      else:
        return 1
    
    # Assign digit lengths
    len1 = countdigits( num1 )
    len2 = countdigits( num2 )
    
    # Check if single digit
    if len1 == 1 or len2 == 1:
        return num1*num2

    # If not, carry on
    else:
        a = num1/( 10**( len1/2 ) )
        b = num1%( 10**( len1/2 ) )
        
        c = num2/( 10**( len2/2 ) )
        d = num2%( 10**( len2/2 ) )
   
        # Compute intermediate values
        ac = karatsuba( a, c )
        bd = karatsuba( b, d )
        abcd = karatsuba( a + b, c + d ) 
       
        # Apply karatsuba's trick
        karatsuba_trick = abcd - ac - bd
       
        # Compute final result
        product = ac  * 10**( len1/2 + len2/2 ) + bd + karatsuba_trick * 10**( (len1 + len2)/4 )
        
    return product 

num1 = 3141592653589793238462643383279502884197169399375105820974944592
num2 = 2718281828459045235360287471352662497757247093699959574966967627

print karatsuba( num1, num2 )
print num1*num2

finish_time = time.time()

print finish_time - start_time
