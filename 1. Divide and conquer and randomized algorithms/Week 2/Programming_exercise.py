#######################################################
############## Sorting algorithms #####################
#######################################################

import numpy as np
import random
import time
import csv

## Load data
with open('Integers.txt', 'rb') as csvfile:
    data = csv.reader(csvfile, delimiter='\n')
    lst = [ int( row[0] ) for row in data ]

# Establish start time
start_time = time.time()

#######################################################
################### Mergesort #########################
#######################################################
def mergesort( lst ):

  count = 0
  ### Base case ###
  if len(lst) <= 1:
    return lst, 0

  ### Recursion ###
  else:
    # Split and recursive call
    a, count1 = mergesort( lst[0:len(lst)/2] )
    b, count2 = mergesort( lst[len(lst)/2:] )
    
    # Glue datasets
    count = count1 + count2
    i = 0; j = 0; 
    orderlst = []
    for k in range( len(lst) ):
      if a[i] <= b[j]:
        orderlst.append( a[i] )
        i = i + 1

        if i == len(a):
          orderlst[len(orderlst):] = b[j:]
          break

      else:
        orderlst.append( b[j] )
        count = count + len(a[i:])
        j = j + 1
        
        if j == len(b):
          orderlst[len(orderlst):] = a[i:]
          break
    
  return orderlst, count

orderedlst, count = mergesort(lst)
times = time.time() - start_time

print orderedlst == sorted(lst)

print times
print "Crosses: ", count
#print "Ordered List: ", orderedlst 

start_time = time.time()

#######################################################
################### Quicksort #########################
#######################################################

def quicksort(lst):

  ## Base case ##
  if len(lst) <= 1:
    return lst
   
  if len(lst) == 2:
    if lst[0] > lst[1]:
      return [lst[1]] + [lst[0]]
    else:
      return lst
 
  ## Processing ##
  k = random.randint(0,len(lst)-1); a = []; b = []; c = []
  for i in range( len(lst) ):
    if lst[k] > lst[i]:
      a[ len(a): ] = [ lst[i] ]
    elif lst[k] == lst[i]:
      b[ len(b): ] = [ lst[i] ]
    elif lst[k] < lst[i]:
      c[ len(c): ] = [ lst[i] ]
  
  return quicksort(a) + b + quicksort(c)

quicksort(lst)

times = time.time() - start_time
print times
print quicksort(lst) == sorted(lst)


thefile = open('test.txt', 'w')

for item in quicksort(lst):
  thefile.write("%s" % item)

