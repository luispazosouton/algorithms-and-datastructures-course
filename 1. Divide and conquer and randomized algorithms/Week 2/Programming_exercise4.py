import random as rand

################ Mergesort algorithm ###########

#### Input = list , output = ordered list ######


def mergesort( lst ):
  
  ## Seed
  if len( lst ) <= 1:
    return lst

  ## Recursive component
  elif len( lst ) > 1:
    a = mergesort( lst[ 0:len(lst)/2 ] )
    b = mergesort( lst[ len(lst)/2: ] )
    return mergepair( a, b )


def mergepair( a, b ):
  
  orderedlst = [0] * len( a + b ); 
  i = 0; j = 0;
  for k in range( len( orderedlst ) ):
    if a[i] <= b[j]:
      orderedlst[k] = a[i]
      i = i + 1

      if i == len(a):
        orderedlst[k+1:] = b[j:]
        break

    elif a[i] > b[j]:
      orderedlst[k] = b[j]
      j = j + 1

      if j == len(b):
        orderedlst[k+1:] = a[i:]
        break
      

  return orderedlst



###### Define quicksort ##########

def quicksort( lst ):

    ### Seed ###
    if len( lst ) <= 1:
        return lst

    ### Recursive part ###
    elif len ( lst ) > 1:
        
        ## Randomization
        pivot = rand.randint(0, len(lst) - 1);
        
        ## Core of algorithm
        a = []; b = []; c = [];
        for k in range( len(lst) ):

            if lst[pivot] > lst[k]:
                a.append( lst[k] )

            elif lst[pivot] < lst[k]:
                b.append( lst[k] )

            elif lst[pivot] == lst[k]:
                c.append( lst[k] )

        return quicksort( a ) + c + quicksort( b )



###### Binary search ##########

### Input = ordered list, Output = Found or not found (Bool)

def binarysearch( lst, num ):
    print lst 
    ## Seed ##
    if len( lst ) < 1:
        return False
    
    elif len( lst ) >= 1:

        ## Define pivot
        pivot = rand.randint( 0, len(lst) -1 )
    
        # Find where it sits in the list
        if num == lst[pivot]:
            return True

        elif num < lst[pivot]:
            return binarysearch( lst[0:pivot], num )

        elif num > lst[pivot]:
            return binarysearch( lst[pivot+1:], num )


### Main ###

lst = map( lambda x: rand.randint(0, 500), [0]*5000 )

print lst
print mergesort( lst )
print quicksort( lst )

num = rand.randint(0,15)
print binarysearch( quicksort( lst ), num )
print num
