import random

################## Mergesort Algorithm ##############

### This function takes a disordered list as an input
### and returns an ordered list as output

## Define recursive function
def mergesort( lst ):

  ## Seed of recursivity
  if len( lst ) <= 1:
    return lst

  ## Define recursion
  else:
    a = mergesort( lst[ 0:len(lst)/2 ] )
    b = mergesort( lst[ len(lst)/2:  ] )
 
    return mergepair( a, b)
   
def mergepair(a , b):
  
  i = 0; j = 0; orderlst = [0] * len( a + b)
  
  for k in range( len( a + b ) ):
    if a[i] <= b[j]:
      orderlst[k] = a[i]
      i = i + 1
      
      if i == len(a):
        orderlst[k+1:] = b[j:]
        break

    elif a[i] > b[j]:
      orderlst[k] = b[j]
      j = j + 1

      if j == len(b):
        orderlst[k+1:] = a[i:]
        break

  return orderlst

## Define quicksort function ##

def quicksort( lst ):

    if len(lst) <= 1:
        return lst

    else:
        a = []; b = []; c = [];
        pivot = random.randint( 0, len(lst)-1 ) 
        for k in range( len(lst) ):

            if lst[k] < lst[pivot]:
                a.append( lst[k] )
            
            elif lst[k] > lst[pivot]:
                b.append( lst[k] )
        
            elif lst[k] == lst[pivot]:
                c.append( lst[k] )

        return quicksort( a ) + c + quicksort( b )

## Define test list ###
lst = [ 3,2,1,5,6,2,1,3,4,8,4,2,1,5,8,3,1,5,6,2,32,23,23,234,612345,784566,1324,51234,1324,73456,345,623,4513,45,457345,662345,6234,523,4 ]

## Print results
print mergesort(lst)
print quicksort(lst)

