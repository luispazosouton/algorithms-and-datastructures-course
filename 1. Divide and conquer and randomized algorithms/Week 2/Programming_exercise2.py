####################### Merge Sort ########################

lst = [1,3,5,2,1,6,1,3]


## This the main mergesort function ##
def mergesort( lst ):
	
    if len(lst) <= 1:
        return lst

    else:
        return mergepair( mergesort( lst[ 0:len(lst)/2 ] ), mergesort( lst[ len(lst)/2: ] ) )


## This function combines leaves ##
def mergepair( a, b ):

    i = 0; j = 0; mergedlst = [0] * len( a + b )
    for k in range( len( a + b ) ):
        
        if a[i] <= b[j]:
            mergedlst[k] = a[i]
            i = i + 1

            if i == len(a):
                mergedlst[k+1:] = b[j:]
                break

        elif a[i] > b[j]:
            mergedlst[k] = b[j]
            j = j + 1

            if j == len(b):
                mergedlst[k+1:] = a[i:]
                break

    return mergedlst


print mergesort(lst)
