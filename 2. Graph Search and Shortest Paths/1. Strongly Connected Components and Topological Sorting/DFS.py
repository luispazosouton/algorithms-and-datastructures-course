import csv
import random as rand

#with open ( 'testcase1.txt' ) as file:
#    edges = [ list( map( int, row ) ) for row in csv.reader( file, delimiter = '\t' ) ]

edges = []
with open ( 'SCC.txt' ) as file:
    for row in csv.reader( file, delimiter = ' ' ):
        edges.append( [ int( elem ) for elem in row if elem != '' ] )

### Function to add an edge to a graph ###
def add_edge_to_graph( graph, edge, directed = False ):
   
    # Add graph nodes from edge
    if edge[0] not in graph:
        graph[ edge[ 0 ] ] = { edge[ 1 ] }
    else:
        graph[ edge[ 0 ] ].add( edge[ 1 ] )

    # Add other side of edge
    if directed:
        if edge[ 1 ] not in graph:
            graph[ edge[ 1 ] ] = set()

    if not directed:
        if edge[ 1 ] not in graph:
            graph[ edge[ 1 ] ] = { edge[ 0 ] }
        else:
            graph[ edge[ 1 ] ].add( edge[ 0 ] )



###### Main #######
graph = {}
for edge in edges: add_edge_to_graph( graph, edge, True )

print( graph )

#### Define Stack for DFS ####

class Stack:
 
    def __init__( self ):
        self.items = []

    def push( self, item ):
        self.items.append( item )

    def pop( self ):
        return self.items.pop()

    def peek( self ):
        return self.items[ len( self.items ) - 1 ]
    
    def size( self ):
        return len( self.items )

    def isempty( self ):
        return self.items == [] 

#### Depth-First-Search ####

# This function takes a graph input and returns the layers
def DFS( graph, Starting_node, Goal ):
    
    # Initialize key elements
    Explored_nodes = { Starting_node }

    # Define stack object
    Stack_of_nodes = Stack()
    Stack_of_nodes.push( Starting_node )

    # Define current label to keep track of ordering
    current_label = len( graph )
    top_order =  []

    # Loop to add layers
    while not Stack_of_nodes.isempty():
        
        # Check if Goal was reached
        if Stack_of_nodes.peek() != Goal:
            break

        # Define children nodes of current node
        options = graph[ Stack_of_nodes.peek() ]

        # If all of them are explored, then backtrack
        if all( x in Explored_nodes for x in options ):
            Stack_of_nodes.pop()
            top_order.insert( 0, current_label )
            current_label -= 1

        # Otherwise, continue exploring
        else:
            next_node = rand.sample( options, 1 )[0]
        
            if next_node not in Explored_nodes:
                Explored_nodes.add( next_node )
                Stack_of_nodes.push( next_node )        


    return Stack_of_nodes.items 

answer = DFS( graph, 1, None )
print( answer )
print( len( answer ) )
