import csv

#with open ( 'directed_graph_edges.txt' ) as file:
#    edges = [ list( map( int, row ) ) for row in csv.reader( file, delimiter = '\t' ) ]

edges = []
with open ( 'SCC.txt' ) as file:
    for row in csv.reader( file, delimiter = ' ' ):
        edges.append( [ int( elem ) for elem in row if elem != '' ] )



### Function to add an edge to a graph ###
def add_edge_to_graph( graph, edge, directed = False ):
   
    # Add graph nodes from edge
    if edge[0] not in graph:
        graph[ edge[ 0 ] ] = { edge[ 1 ] }
    else:
        graph[ edge[ 0 ] ].add( edge[ 1 ] )

    # Add other side of edge
    if directed:
        if edge[ 1 ] not in graph:
            graph[ edge[ 1 ] ] = set()

    if not directed:
        if edge[ 1 ] not in graph:
            graph[ edge[ 1 ] ] = { edge[ 0 ] }
        else:
            graph[ edge[ 1 ] ].add( edge[ 0 ] )


###### Main #######
graph = {}
for edge in edges: add_edge_to_graph( graph, edge, directed = True )

graph_rev = {}
for edge in edges: add_edge_to_graph( graph_rev, [ edge[1], edge[0] ],directed = True )

##### DFS Loop ##########

def DFS_Loop( graph ):
    
    # First Pass
    explored_nodes = set() 
    finishing_times = {} 
    t = 0
    leader = dict() 
    for node in graph_rev:
        if node not in explored_nodes:
            s = node
            s, t = DFS( graph_rev, node, finishing_times, s, t, leader, explored_nodes )

    # Second Pass
    explored_nodes = set() 
    t = 0
    leader = dict()
    node_order = list( finishing_times.keys() )[::-1]
    for node in node_order:
        if node not in explored_nodes:
            s = node
            s, t = DFS( graph, node, finishing_times, s, t, leader, explored_nodes )
   
    # Process data
    result = dict()
    for item in leader.items():
        if item[ 1 ] not in result:
            result[ item[ 1 ] ] = { item[ 0 ] }
        else:
            result[ item[ 1 ] ].add( item[ 0 ] )

    return len( result ) 



# Define DFS
def DFS( graph, start, finishing_times, s, t, leader, explored_nodes = set() ):
    
    # Initialize key elements
    explored_nodes.add( start )
    leader[ start ] = s
    for child_node in graph[ start ]:
        if child_node not in explored_nodes:
            s, t = DFS( graph, child_node, finishing_times, s, t, leader, explored_nodes )
    t += 1 
    finishing_times[ start ] = t 
    return [ s, t ] 



#################### Main #################

print( DFS_Loop( graph ) )
