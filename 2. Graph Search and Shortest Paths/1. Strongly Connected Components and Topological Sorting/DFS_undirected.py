import csv
import random as rand

#with open ( 'graph_edges.txt' ) as file:
#    edges = [ list( map( int, row ) ) for row in csv.reader( file, delimiter = '\t' ) ]

edges = []
with open ( 'SCC.txt' ) as file:
    for row in csv.reader( file, delimiter = ' ' ):
        edges.append( [ int( elem ) for elem in row if elem != '' ] )

### Function to add an edge to a graph ###
def add_edge_to_graph( graph, edge, directed = False, stop = 0 ):
   
    # Add graph nodes from edge
    if edge[0] not in graph:
        graph[ edge[ 0 ] ] = { edge[ 1 ] }
    else:
        graph[ edge[ 0 ] ].add( edge[ 1 ] )
    
    if not directed:
        # Decide if continue
        if stop > 0:
           return 
        
        # Call function again with reversed edge
        add_edge_to_graph( graph, edge[ :: -1 ], 1 )



###### Main #######
graph = {}
for edge in edges: add_edge_to_graph( graph, edge, True )

#### Define Stack for DFS ####

class Stack:
 
    def __init__( self ):
        self.items = []

    def push( self, item ):
        self.items.append( item )

    def pop( self ):
        return self.items.pop()

    def peek( self ):
        return self.items[ len( self.items ) - 1 ]
    
    def size( self ):
        return len( self.items )

    def isempty( self ):
        return self.items == [] 

#### Breadth-First-Search ####

# This function takes a graph input and returns the layers
def DFS( graph, Starting_node, Goal ):

    # Initialize key elements
    Explored_nodes = { Starting_node }

    # Define stack object
    Stack_of_nodes = Stack()
    Stack_of_nodes.push( Starting_node )
    
    # Loop to add layers
    while Stack_of_nodes.peek() != Goal and len( Explored_nodes ) != len( graph ):
       
        # Define children nodes of current node
        options = graph[ Stack_of_nodes.peek() ]

        # If all of them are explored, then backtrack
        if all( x in Explored_nodes for x in options ):
            Stack_of_nodes.pop()

        # Otherwise, continue exploring
        else:
            next_node = rand.sample( options, 1 )[0]
        
            if next_node not in Explored_nodes:
                Explored_nodes.add( next_node )
                Stack_of_nodes.push( next_node )        


    return Stack_of_nodes.items 

answer = DFS( graph, 1, 10000 )
print( answer )
print( len( answer ) )
