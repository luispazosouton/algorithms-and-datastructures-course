import csv
import random as rand

with open ( 'graph_edges.txt' ) as file:
    edges = [ list( map( int, row ) ) for row in csv.reader( file, delimiter = '\t' ) ]

#edges = []
#with open ( 'SCC.txt' ) as file:
#    for row in csv.reader( file, delimiter = ' ' ):
#        edges.append( [ int( elem ) for elem in row if elem != '' ] )


### Function to add an edge to a graph ###
def add_edge_to_graph( graph, edge, directed = False ):
   
    # Add graph nodes from edge
    if edge[0] not in graph:
        graph[ edge[ 0 ] ] = { edge[ 1 ] }
    else:
        graph[ edge[ 0 ] ].add( edge[ 1 ] )

    # Add other side of edge
    if directed:
        if edge[ 1 ] not in graph:
            graph[ edge[ 1 ] ] = set()

    if not directed:
        if edge[ 1 ] not in graph:
            graph[ edge[ 1 ] ] = { edge[ 0 ] }
        else:
            graph[ edge[ 1 ] ].add( edge[ 0 ] )



###### Main #######
graph = {}
for edge in edges: add_edge_to_graph( graph, edge, True )

#### Define Stack for DFS ####

class Stack:
 
    def __init__( self ):
        self.items = []

    def push( self, item ):
        self.items.append( item )

    def pop( self ):
        return self.items.pop()

    def peek( self ):
        return self.items[ len( self.items ) - 1 ]
    
    def size( self ):
        return len( self.items )

    def isempty( self ):
        return self.items == [] 



# DFS Loop
def DFS_Loop( graph ):
    explored_nodes = set() 
    current_label = [ len( graph ) ]
    f = {} 

    for node in graph:
        if node not in explored_nodes:
            DFS( graph, node, current_label, f, explored_nodes )

    return f


def DFS( graph, starting_node, current_label, f, explored_nodes = set() ):
    
    # Initialize key elements
    explored_nodes.add( starting_node )
    for child_node in graph[ starting_node ]:
        if child_node not in explored_nodes:
            DFS( graph, child_node, current_label, f, explored_nodes )
    
    f[ starting_node ] = current_label[0]
    current_label[0] -= 1

    return explored_nodes

answer2 = DFS_Loop( graph )
print( answer2 )

#answer = DFS( graph, 1, None )
#print( answer )
#print( len( answer ) )
