import csv

with open ( 'graph_edges.txt' ) as file:
    edges = [ list( map( int, row ) ) for row in csv.reader( file, delimiter = '\t' ) ]

#edges = []
#with open ( 'SCC.txt' ) as file:
#    for row in csv.reader( file, delimiter = ' ' ):
#        edges.append( [ int( elem ) for elem in row if elem != '' ] )

### Function to add an edge to a graph ###
def add_edge_to_graph( graph, edge, stop = 0 ):
   
    # Add graph nodes from edge
    if edge[0] not in graph:
        graph[ edge[ 0 ] ] = { edge[ 1 ] }
    else:
        graph[ edge[ 0 ] ].add( edge[ 1 ] )
    
    # Decide if continue
    if stop > 0:
       return 
    
   # Call function again with reversed edge
    add_edge_to_graph( graph, edge[ :: -1 ], 1 )



###### Main #######
graph = {}
for edge in edges: add_edge_to_graph( graph, edge )

#### Define Queue for BFS ####

class Queue:
    def __init__( self ):
        self.items = []

    def enqueue( self, item ):
        self.items.insert(0,item)

    def dequeue( self ):
        return self.items.pop()

    def size( self ):
        return len( self.items )

#### Breadth-First-Search ####

# This function takes a graph input and returns the layers
def BFS( graph, start ):

    # Initialize key elements
    explored_nodes = { start }
    List_of_layers = [ [ start ] ]
    dist = { start: 0 }
    
    # Define queue object
    Queue_nodes = Queue()
    Queue_nodes.enqueue( start )
    
    # Loop to add layers
    while Queue_nodes.size() > 0:
        
        v = Queue_nodes.dequeue()
        for w in graph[ v ]:
            if w not in explored_nodes:
                explored_nodes.add( w )
                Queue_nodes.enqueue( w ) 
                dist[ w ] = dist[ v ] + 1

    return dist

print( BFS( graph, 1 ) )
