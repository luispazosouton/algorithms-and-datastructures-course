import csv
import random

#with open ( 'directed_graph_edges.txt' ) as file:
#    edges = [ list( map( int, row ) ) for row in csv.reader( file, delimiter = '\t' ) ]

#with open ( 'testcase2.txt' ) as file:
#    edges = [ list( map( int, row ) ) for row in csv.reader( file, delimiter = ' ' ) ]

edges = []
with open ( 'SCC.txt' ) as file:
    for row in csv.reader( file, delimiter = ' ' ):
        edges.append( [ elem for elem in row if elem != '' ] )

### Function to add an edge to a graph ###
def add_edge_to_graph( graph, edge, directed = False ):
   
    # Add graph nodes from edge
    if edge[0] not in graph:
        graph[ edge[ 0 ] ] = { edge[ 1 ] }
    else:
        graph[ edge[ 0 ] ].add( edge[ 1 ] )

    # Add other side of edge
    if directed:
        if edge[ 1 ] not in graph:
            graph[ edge[ 1 ] ] = set()

    if not directed:
        if edge[ 1 ] not in graph:
            graph[ edge[ 1 ] ] = { edge[ 0 ] }
        else:
            graph[ edge[ 1 ] ].add( edge[ 0 ] )

#### Define Stack for DFS ####

class Stack:
 
    def __init__( self ):
        self.items = []

    def push( self, item ):
        self.items.append( item )

    def pop( self ):
        return self.items.pop()

    def peek( self ):
        return self.items[ len( self.items ) - 1 ]
    
    def size( self ):
        return len( self.items )

    def isempty( self ):
        return self.items == [] 


###### Main #######
graph = dict()
for edge in edges: add_edge_to_graph( graph, edge, directed = True )

graph_rev = dict()
for edge in edges: add_edge_to_graph( graph_rev, [ edge[1], edge[0] ], directed = True )

#### Define Stack for DFS ####
class Stack:
 
    def __init__( self ):
        self.items = []

    def push( self, item ):
        self.items.append( item )

    def pop( self ):
        return self.items.pop()

    def peek( self ):
        return self.items[ len( self.items ) - 1 ]
    
    def size( self ):
        return len( self.items )

    def isempty( self ):
        return self.items == [] 

##### DFS Loop ##########

def DFS_Loop( graph ):
    
    ##### First Pass of DFS #####
    explored_nodes = set() 
    finishing_times = {} 
    t = 0
    leader = dict()
    list_graph = random.sample( graph_rev.keys(), len( graph_rev) )
    for node in list_graph:
        if node not in explored_nodes:
            t = DFS( graph_rev, node, finishing_times, t, leader, explored_nodes )

    ##### Second Pass of DFS #####
    explored_nodes = set() 
    leader = dict()
    node_order = list( finishing_times.keys() )[::-1]
    for node in node_order:
        if node not in explored_nodes:
            DFS( graph, node, finishing_times, t, leader, explored_nodes )
   
    #### Reverse dictionary of leaders  ###
    result = dict()
    for item in leader.items():
   
        #print( item )
        if item[ 1 ] not in result:
            result[ item[ 1 ] ] = { item[ 0 ] }
        else:
            result[ item[ 1 ] ].add( item[ 0 ] )
    
    # Count 5 largest SCCs
    result_list = [ [key, len( value )] for key, value in result.items() ]
    final_results = [ 0 ] *10 
    minimval, idx = 0, 0
    new_leaders = [ 0 ] * 10
    for elem in result_list:
        if elem[1] > minimval:
            final_results[ idx ] = elem[1]
            new_leaders[ idx ] = elem
            minimval, idx = min( (minimval, idx) for (idx, minimval) in enumerate( final_results ) )
    
    return [ len( result ), sorted( final_results, reverse = True ) ]


# This function performs DFS using a stack 
def DFS( graph, start, finishing_times, time, leader, explored_nodes = set()  ):

    # Initialize key elements
    explored_nodes.add( start )

    # Define stack object
    Stack_of_nodes = Stack()
    Stack_of_nodes.push( start )

    # Loop to add layers
    while not Stack_of_nodes.isempty():
        
        # Define children nodes of current node
        options = graph[ Stack_of_nodes.peek() ]
        
        # If all of them are explored, then backtrack
        if len( options ) == 0 or all( elem in explored_nodes for elem in options ):
            
            # Remove last element
            removed_node = Stack_of_nodes.pop()
            
            # Collect time of completion
            time += 1
            finishing_times[ removed_node ] = time
            leader[ removed_node ] = start

        # If not fully explored, continue
        else:
            for node in options:
                if node not in explored_nodes:
                    explored_nodes.add( node )
                    Stack_of_nodes.push( node )        

    # Return finishing times
    return time 


#################### Main #################

print( DFS_Loop( graph ) )
