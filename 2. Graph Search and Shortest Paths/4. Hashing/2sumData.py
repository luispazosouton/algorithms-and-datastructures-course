"""
The goal of this problem is to implement a variant of the 2-SUM algorithm.

The file named 2sumData.txt contains 1 million integers, both positive and negative (there might be some repetitions!).This is your array of integers, with the ithi^{th}ith row of the file specifying the ithi^{th}ith entry of the array.

Your task is to compute the number of target values ttt in the interval [-10000,10000] (inclusive) such that there are distinct numbers x,yx,yx,y in the input file that satisfy x+y=tx+y=tx+y=t.
"""


## Simple 2-sum algorithm
import csv,time
from multiprocessing.dummy import Pool as ThreadPool


##### Define two-sum function #######
def twoSum( iterable, target ):
    
    # Define set of substractions
    cache = set()

    # Loop through iterable
    for x in iterable:
        
        # Check if in cache
        if x in cache:
            return 1
        
        # Otherwise add to cache
        cache.add( target - x )
    
    # If nothing found
    return 0


#################################################
################# Main ##########################
#################################################


##### Load data onto a set to remove duplicates, and then onto tuple for better memory management ####
with open('2sumData.txt') as file:
    inputNumbers = tuple( { int( row[0] ) for row in csv.reader( file ) } )

# Take time before loop
time0 = time.time()
    
# Instatiate object with 8 parallel threads
pool = ThreadPool( 8 )

# Run two sum function over target range
counter = sum( pool.map( lambda target: twoSum( inputNumbers, target ), range( -10000, 10001 ) ) )
pool.close()
pool.join()

# Print final results
print( 'Computation time: ', round( time.time() - time0, 1 ), ' Seconds' )
print( 'Number of matches: ', counter )
