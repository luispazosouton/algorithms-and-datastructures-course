### Program a heap class to then use it to keep track of the median in an incoming flow of numbers
class Heap():

    def __init__( self ):
        self.array = []
        self.size = 0

    def insert( self, key ):

        # Define bubbleUp subfunction
        def bubbleUp( self, index ):
            
            # If reached root, stop recursion
            if index == 0:
                return 

            # If Heap condition is fullfilled stop recursion
            parentIndex = self.getParentIndex( index )
            if self.array[ index ] > self.array[ parentIndex ]:
                return
            
            # If Heap condition is violated, swap child and parent
            self.array[ index ], self.array[ parentIndex ] =  self.array[ parentIndex ], self.array[ index ]

            # Recursive call
            bubbleUp( self, parentIndex ) 
        
        
        ############ Main Function ###########

        # First insert new key in the last position of array
        self.array.append( key )
        
        # Get it's index and bubble up
        index = self.getSize() - 1
        
        # Bubble up
        bubbleUp( self, index )

    def extractRoot( self ):
        
        if self.isEmpty():
            raise Exception( 'The heap is empty' )

        elif len( self.array ) == 1:
            return self.array.pop()

        # For any other case
        self.array[ 0 ], self.array[ -1 ] = self.array[ -1 ], self.array[ 0 ]

        # Extract min value and store
        minVal = self.array.pop()

        ### Now Bubble Down ###
        def bubbleDown( self, index ):
        
            # Extract children and check that are within array
            indexChild1, indexChild2 = self.getChildrenIndex( index )
            maxIndex = self.getSize() - 1

            # If no available children, stop recursion
            if indexChild1 > maxIndex and indexChild2 > maxIndex:
                return

            # If only one one child
            elif indexChild1 > maxIndex or indexChild2 > maxIndex:
                smallerChildIndex = min( indexChild1, indexChild2 )
            
            # Else if both children exist, select the one with smallest key
            else:
                smallerChildIndex = min( [ indexChild1, indexChild2], key = lambda x: self.array[ x ] )

                if self.array[ index ] < self.array[ smallerChildIndex ]:
                    return
                
            # Swap positions
            self.array[ index ], self.array[ smallerChildIndex ] = self.array[ smallerChildIndex ], self.array[ index ] 
            
            # Recurse
            bubbleDown( self, smallerChildIndex )
        
        # Execute bubble down
        bubbleDown( self, 0 )

        # Return minimum value
        return minVal

    def peek( self ):
        if self.isEmpty():
            raise Exception( 'The heap is empty' )

        return self.array[0]

    def getSize( self ):
        return len( self.array )

    def getParentIndex( self, index ):
        index += 1
        return index//2 - 1

    def getChildrenIndex( self, index ):
        index += 1
        return ( index * 2 - 1, index * 2 )

    def isEmpty( self ):
        return self.getSize() == 0




################# Import data ##########################
import csv
with open('Median.txt') as file:
    inputNumbers = []
    for row in csv.reader( file ):
        inputNumbers.append( int( row[0] ) )

# Instantiate heap objects
heapMin = Heap()
heapMax = Heap()

# Compute the median for incoming numbers
for number in inputNumbers:
    
    if heapMin.getSize() + heapMax.getSize() == 0:
        heapMin.insert( number )
        median = sumMedian = number
        continue
    
    # When the heaps are not empty
    if number >= median:
        heapMin.insert( number )

    elif number < median:
        heapMax.insert( -number )

    ## Now decide who is the new median
    # If equal, then the median is the smallest in the min heap
    if heapMax.getSize() == heapMin.getSize():
        median = -heapMax.peek()
    
    # If more items in the max Heap
    elif heapMax.getSize() > heapMin.getSize():

        if heapMax.getSize() - heapMin.getSize() == 1:
            median = -heapMax.peek()

        elif heapMax.getSize() - heapMin.getSize() == 2:
            heapMin.insert( - heapMax.extractRoot() )
            median = -heapMax.peek()

    # Else if more items in the min Heap
    elif heapMax.getSize() < heapMin.getSize():

        if heapMin.getSize() - heapMax.getSize() == 1:
            median = heapMin.peek()

        elif heapMin.getSize() - heapMax.getSize() == 2:
            heapMax.insert( - heapMin.extractRoot() )
            median = -heapMax.peek()
    
    sumMedian += median

### Print final result 
print( sumMedian % 10000 )
