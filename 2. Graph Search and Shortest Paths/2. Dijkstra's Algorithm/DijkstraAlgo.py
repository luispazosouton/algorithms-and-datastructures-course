# Compute distance from source vertex 1 to all other vertices
import csv

# Function to read file of data
def getTuple( string ):

    index = string.find( ',' )
    return  ( int( string[ :index ] ), int( string[ index + 1: ] ) )


# Read data from file and create graph
graph = dict()
with open('Dijkstra_data.txt') as file:
    for row in csv.reader(file, delimiter='\t'):
        graph[ int( row[ 0 ] ) ] = [ getTuple( elem ) for elem in row[ 1:-1] ]


#### Define Dijkstra's Algorithm ########

#### First run DFS to remove non-reachable vertices ####
def DFS( root ):

    # Define stack
    stack = [ root ]
    reachableNodes = set() 
    while stack:

        # Extract current node
        node = stack.pop()
        
        if node not in reachableNodes:
            
            # Add node to set of reachable nodes
            reachableNodes.add( node )

            # Add elements to stack
            stack.extend( [ elem[0] for elem in graph[ node ] ] )
    
    return reachableNodes


# Simple function to compute the Dijkstra's criteria
def dijkstraLength( edge ):
    sourceVertex, newVertex, distance = edge
    return ( A[ sourceVertex ] + distance, newVertex )



#######################################################################################################
########################################## Main #######################################################
#######################################################################################################

# Define set of visited vertices and hashtable of distances
source = 1
X = { source }
A = { source : 0 }
V = DFS( source )

# Run until all vertices are visited
while X != V:

    filteredEdges = []
    for v in X:
        filteredEdges.extend( [ (v, newVertex, distance) for newVertex, distance in graph[ v ] if newVertex in V - X ] )
    
    # Select the edge that minimizes the criteria
    distance, newVertex = min( map( dijkstraLength, filteredEdges ), key = lambda x: x[0] ) 
    
    # Save values
    X.add( newVertex )
    A[ newVertex ] = distance


# Obtain distances for indicated vertices
results = [ 7,37,59,82,99,115,133,165,188,197 ]

finalVals = [ A[ vertex ] for vertex in results ]
print( finalVals )


